package com.greglturnquist.payroll;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    @DisplayName("exist job title")
    void getJobTitle_jobTitleExist() {
        //Arrange
        Employee employeeA =new Employee("firstname A","lastName A","description A", "jobTitle A");

        //Act
        String expectedResult="jobTitle A";
        String realResult=employeeA.getJobTitle();

        //Assert
        assertEquals(expectedResult,realResult);
    }

    @Test
    @DisplayName("not exist job title")
    void getJobTitle_jobTitleNotExist() {
        //Arrange
        Employee employeeA =new Employee("firstname A","lastName A","description A", "");

        //Act
        String expectedResult="";
        String realResult=employeeA.getJobTitle();

        //Assert
        assertEquals(expectedResult,realResult);
    }

    @Test
    @DisplayName("null job title")
    void getJobTitle_jobTitleNull() {
        //Arrange
        Employee employeeA =new Employee("firstname A","lastName A","description A",null);

        //Act
        String expectedResult=null;
        String realResult=employeeA.getJobTitle();

        //Assert
        assertEquals(expectedResult,realResult);
    }

    @Test
    @DisplayName("set null to jobtitle A")
    void setJobTitle_existJobTitle() {
            //Arrange
            Employee employeeA =new Employee("firstname A","lastName A","description A",null);

            //Act
            String expectedResult="jobTitle A";

            employeeA.setJobTitle("jobTitle A");
            String realResult=employeeA.getJobTitle();

            //Assert
            assertEquals(expectedResult,realResult);
    }
    @Test
    @DisplayName("set jobTitle A to jobtitle B")
    void setJobTitle_existJobTitleAtoB () {
        //Arrange
        Employee employeeA =new Employee("firstname A","lastName A","description A","jobTitle A");

        //Act
        String expectedResult="jobTitle B";

        employeeA.setJobTitle("jobTitle B");
        String realResult=employeeA.getJobTitle();

        //Assert
        assertEquals(expectedResult,realResult);
    }
    @Test
    @DisplayName("set jobTitle B to jobtitle null")
    void setJobTitle_existJobTitleBtoNull () {
        //Arrange
        Employee employeeA =new Employee("firstname A","lastName A","description A","jobTitle B");

        //Act
        String expectedResult=null;

        employeeA.setJobTitle(null);
        String realResult=employeeA.getJobTitle();

        //Assert
        assertEquals(expectedResult,realResult);
    }
}